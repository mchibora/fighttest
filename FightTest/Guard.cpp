#include "Guard.h"

Guard::Guard()
{
	bulletSpeed = 13;
	shape.setRadius(5.f);
	shape.setFillColor(sf::Color::Yellow);

	//sprite.setOrigin(32, 32);
}

sf::CircleShape* Guard::getSprite()
{
	return &shape;
}

sf::Vector2f Guard::getDirection()
{
	return direction;
}

void Guard::setDirection(float x, float y)
{
	direction.x = bulletSpeed*x;
	direction.y = bulletSpeed*y;
}

void Guard::setPosition(sf::Vector2f passedPosition)
{
	shape.setPosition(passedPosition);
}

bool Guard::isOffScreen(int screenHeight, int screenWidth)
{
	if (shape.getPosition().x<0 || shape.getPosition().x>screenWidth || shape.getPosition().y<0 || shape.getPosition().y>screenHeight)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Guard::move()
{
	shape.move(direction);
}

sf::FloatRect Guard::getBoundBox()
{
	return shape.getGlobalBounds();
}