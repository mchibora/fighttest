#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(1024, 683), "FightTest");
	window.setVerticalSyncEnabled(true);

	/*sf::Image fighterOneSheet;
	fighterOneSheet.loadFromFile("RyuSheet.png");
	int fighterLength = 66;
	int fighterHeight = 109;
	int currentFighterSprite = 0;*/

	sf::Texture ortonRunBigTexture;
	ortonRunBigTexture.loadFromFile("ortonRun.png");
	sf::Sprite ortonSprite;
	ortonSprite.setTexture(ortonRunBigTexture);
	ortonSprite.setOrigin(16, 32);
	sf::Texture ortonStandBigTexture;
	ortonStandBigTexture.loadFromFile("ortonStand.png");
	sf::Sprite ortonStandSprite;
	ortonStandSprite.setTexture(ortonStandBigTexture);
	ortonStandSprite.setOrigin(85, 193);
	sf::Texture RKOBigTexture;
	RKOBigTexture.loadFromFile("RKO.png");
	sf::Sprite RKOSprite;
	RKOSprite.setTexture(RKOBigTexture);
	RKOSprite.setOrigin(250, 201);

	sf::Texture guardTexture;
	guardTexture.loadFromFile("guardSide.png");
	sf::Sprite guardSprite;
	guardSprite.setTexture(guardTexture);
	guardSprite.setOrigin(16, 32);
	guardSprite.setPosition(250, 201);
	sf::Texture guardTexture2;
	guardTexture2.loadFromFile("guardUp.png");
	sf::Sprite guardSprite2;
	guardSprite2.setTexture(guardTexture2);
	guardSprite2.setOrigin(16, 32);
	guardSprite2.setPosition(300, 301);
	sf::Texture guardTexture3;
	guardTexture3.loadFromFile("guardDown.png");
	sf::Sprite guardSprite3;
	guardSprite3.setTexture(guardTexture3);
	guardSprite3.setOrigin(16, 32);
	guardSprite3.setPosition(450, 401);

	ortonSprite.setTexture(ortonStandBigTexture);
	bool runSide = true;
	bool RKO = false;
	bool RKOend = false;
	int animInterval = 6;
	

	sf::Texture fighterOneTexture;
	//sf::IntRect spriteBox(((currentFighterSprite % 9))*fighterLength, ((currentFighterSprite % 6))*fighterHeight, fighterLength, fighterHeight);
	//fighterOneTexture.loadFromImage(fighterOneSheet, spriteBox);
	sf::Sprite fighterOneSprite;
	//fighterOneSprite.setTexture(fighterOneTexture);
	fighterOneSprite.setPosition(300, 500);

	sf::Vector2i topLefts[64];
	sf::Vector2i lengthHeight[64];

	int animationCounter = 0;
	int jumpCounter = 0;
	int jumpVelo = -6;
	int jumpTime = 0;
	bool jumping = false;
	int jumpAccel = 1;

	//TESTING VALUES. Should make a text file probably
	topLefts[0] = sf::Vector2i(0, 0);
	lengthHeight[0] = sf::Vector2i(66, 109);
	topLefts[28] = sf::Vector2i(85, 312);
	lengthHeight[28] = sf::Vector2i(148-85, 425-312);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		ortonSprite.setTexture(ortonStandBigTexture);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			ortonSprite.setTexture(ortonRunBigTexture);
			if(!RKO)
				ortonSprite.move(5, 0);
			if (animationCounter++ > animInterval && !RKO)
			{
				animationCounter = 0;
				if (runSide)
				{
					ortonSprite.setScale(-1.f, 1.f);
					runSide = false;
				}
				else
				{
					ortonSprite.setScale(1.f, 1.f);
					runSide = true;
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			ortonSprite.setTexture(ortonRunBigTexture);
			if (!RKO)
				ortonSprite.move(-5, 0);
			if (animationCounter++ > animInterval && !RKO)
			{
				animationCounter = 0;
				if (runSide)
				{
					ortonSprite.setScale(-1.f, 1.f);
					runSide = false;
				}
				else
				{
					ortonSprite.setScale(1.f, 1.f);
					runSide = true;
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			ortonSprite.setTexture(ortonRunBigTexture);
			if (!RKO)
				ortonSprite.move(0, -5);
			if (animationCounter++ > animInterval && !RKO)
			{
				animationCounter = 0;
				if (runSide)
				{
					ortonSprite.setScale(-1.f, 1.f);
					runSide = false;
				}
				else
				{
					ortonSprite.setScale(1.f, 1.f);
					runSide = true;
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			ortonSprite.setTexture(ortonRunBigTexture);
			if (!RKO)
				ortonSprite.move(0, 5);
			if (animationCounter++ > animInterval && !RKO)
			{
				animationCounter = 0;
				if (runSide)
				{
					ortonSprite.setScale(-1.f, 1.f);
					runSide = false;
				}
				else
				{
					ortonSprite.setScale(1.f, 1.f);
					runSide = true;
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			
			ortonSprite.setRotation(90);
			RKO = true;
			ortonSprite.setTexture(RKOBigTexture);
			if (runSide)
			{
				ortonSprite.setScale(-1.f, 1.f);
				runSide = false;
			}
		}
		else
		{
			ortonSprite.setRotation(0);
			RKO = false;
		}


		/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			if (!jumping)
			{
				jumping = true;
				//jumpVelo = 50;
				jumpTime = 0;
				currentFighterSprite = 28;
			}
		}

		if (jumping)
		{

			jumpTime++;
			int calcedMove = jumpVelo*jumpTime + 0.5*jumpAccel*jumpTime*jumpTime;
			fighterOneSprite.move(0, calcedMove);

			if (fighterOneSprite.getPosition().y > 500)
			{
				fighterOneSprite.setPosition(fighterOneSprite.getPosition().x, 500);
				jumping = false;
				currentFighterSprite = 0;
			}
		}

		//sf::IntRect spriteBox(((currentFighterSprite % 9))*fighterLength, ((currentFighterSprite % 6))*fighterHeight, fighterLength, fighterHeight);
		sf::IntRect spriteBox(topLefts[currentFighterSprite], lengthHeight[currentFighterSprite]);
		fighterOneTexture.loadFromImage(fighterOneSheet, spriteBox);
		fighterOneSprite.setTexture(fighterOneTexture);*/

		window.clear();
		sf::FloatRect ortonBox = ortonSprite.getGlobalBounds();
		sf::FloatRect guardBox = guardSprite.getGlobalBounds();
		if ((ortonBox.intersects(guardBox) && RKO))
		{
			guardSprite.setRotation(-90);
		}
		
		window.draw(guardSprite);
		window.draw(guardSprite2);
		window.draw(guardSprite3);
		window.draw(ortonSprite);
		window.display();
	}

	return 0;
}