#ifndef GUARD_H
#define GUARD_H

#include <SFML/Graphics.hpp>

class Guard
{
private:
	int bulletSpeed;
	sf::Vector2f direction;
	sf::CircleShape shape;

public:
	Guard();
	sf::CircleShape* getSprite();
	sf::Vector2f getDirection();
	void setDirection(float x, float y);
	void setPosition(sf::Vector2f passedPosition);
	bool isOffScreen(int screenHeight, int screenWidth);
	void move();
	sf::FloatRect getBoundBox();
};
#endif
